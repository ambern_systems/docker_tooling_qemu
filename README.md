# Docker Tooling for QEMU environments

## Version 0.1

Just some simple quick development aids for quickly spinning up docker
containers intended for cross development. Architectures added as needed.

The purpose is to allow low-fuss cross-platform development without the
whole weight or limits of a full VM and with a consistent repeatable dev and
test environment.

It works by defining a container that operates as a Ubuntu 18.04 environment
configured for cross-compile/emulation of a specific target.

## Currently Supports

 - An 'all-you-can-eat' cross-compile environment
 - Risc-V 64-bit softmmu

## Limitations

Because this is primarily for dev work on desktop/powerful laptops it is
intended not to be very CPU/mem intensive, hence builds are conservative about
using available threads.

## Usage

In one terminal:

        spin.h <environment> [local_code_dir] [command]

Will build (if required, otherwise restart) a docker environment configured
as a clean, known Ubuntu OS with cross-compile tools installed. This includes
a qemu environment (which may require cloning and building depending on the
target).

Once complete it will either execute 'command' or idle. If in 'idle' it will
run until you enter CTRL-C (which should trigger a graceful shutdown) in the
initial terminal, otherwise it will exit once 'command' completes.

*Important!* The command cannot be interactive!

Then use:

        connect.sh <environment> [command]

This opens a bash shell inside the container that allows you to trigger builds
or run tests as needed.

As it uses Docker 'volumes' mounted from your local filesystem you can edit
code or tests on the host machine while developing and because docker allows
quick spinning up of the environment on any machine you can clone this project
to it allows quick test automation as well.

## Examples:

### Full Risc64 Linux VM
Build a risc64-softmmu environment and run linux:

First Terminal:

        ./spin.sh risc64-softmmu

Second Terminal:

        ./connect.sh risc64-softmmu
        /scripts/run_risc64.sh

Third Terminal

        make # The default code directory
        /scripts/scp_to_qemu.sh test* .


Copyright © 2018 Ambern Systems
