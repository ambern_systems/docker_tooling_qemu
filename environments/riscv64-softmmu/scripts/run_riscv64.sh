#!/usr/bin/env bash

/tools/qemu-system-riscv64 \
       -nographic \
       -machine virt \
       -smp 4 \
       -m 2G \
       -kernel /tools/bbl \
       -object rng-random,filename=/dev/urandom,id=rng0 \
       -device virtio-rng-device,rng=rng0 \
       -append "console=ttyS0 ro root=/dev/vda1" \
       -device virtio-blk-device,drive=hd0 \
       -drive file=/tools/Fedora-Developer-Rawhide-20181007.n.0-sda.raw,format=raw,id=hd0 \
       -device virtio-net-device,netdev=usernet \
       -netdev user,id=usernet,hostfwd=tcp::10000-:22
