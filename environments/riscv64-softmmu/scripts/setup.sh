#!/usr/bin/env bash
#
# Copyright © 2018 Ambern Systems
#
# This file is part of docker_tooling_qemu.
#
# docker_tooling_qemu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# docker_tooling_qemu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with docker_tooling_qemu.  If not, see <https://www.gnu.org/licenses/>.
#

if [ ! -d /tools/riscv-qemu ]; then
  cd /tools && git clone https://github.com/ucb-bar/riscv-qemu.git
fi

if [ ! -f /tools/riscv-qemu/riscv32-softmmu/qemu-system-riscv32 ]; then
  cd /tools/riscv-qemu
  ./configure --target-list=riscv64-softmmu,riscv32-softmmu && make
fi

if [ ! -f /tools/qemu-system-riscv32 ]; then
  cd /tools
  ln -s riscv-qemu/riscv32-softmmu/qemu-system-riscv32 qemu-system-riscv32
  ln -s riscv-qemu/riscv64-softmmu/qemu-system-riscv64 qemu-system-riscv64
fi

if [ ! -f /tools/Fedora-Developer-Rawhide-20181007.n.0-sda.raw ]; then
  cd /tools
  curl -o Fedora-Developer-Rawhide-20181007.n.0-sda.raw.xz http://fedora-riscv.tranquillity.se/kojifiles/work/tasks/2608/112608/Fedora-Developer-Rawhide-20181007.n.0-sda.raw.xz
  xzdec -d Fedora-Developer-Rawhide-20181007.n.0-sda.raw.xz > Fedora-Developer-Rawhide-20181007.n.0-sda.raw
fi

if [ ! -f /tools/bbl ]; then
  cd /tools
  curl -o bbl https://fedorapeople.org/groups/risc-v/disk-images/bbl
fi

echo "Risc-V QEMU Environment ${QEMU_ENV} ready to emu"
echo " Use '/scripts/run_riscv64.sh' to run full Risv-V Linux Environment"

if [ "$DEFAULT_COMMAND" == "" ];
then
  while [ true ]
  do
    sleep 60
  done
else
  $DEFAULT_COMMAND
fi
