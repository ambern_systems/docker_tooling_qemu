#!/usr/bin/env bash
#
# Copyright © 2018 Ambern Systems
#
# This file is part of docker_tooling_qemu.
#
# docker_tooling_qemu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# docker_tooling_qemu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with docker_tooling_qemu.  If not, see <https://www.gnu.org/licenses/>.
#

export QEMU_ENV=${1:-missing}
shift
LOCAL_CODE=${1:-"./environments/${QEMU_ENV}/defaultcode"}
shift
export DEFAULT_COMMAND="$@"

export BASE=`pwd`

export LOCAL_CODE=${BASE}/${LOCAL_CODE}

echo "Target Environment: ${QEMU_ENV}"
echo "  Using: ${LOCAL_CODE} as /code"
if [ -d environments/${QEMU_ENV} ]; then
  cd environments/${QEMU_ENV}
  docker-compose up --build qemu_env
else
  echo "Available environments:"
  echo `ls -1 environments`
  echo "Run with './spin.sh <environment>'"
fi
